# README #

### STEPS TO BOOTSTRAP THIS PROJECT###
1. npm install or yarn install
2. npm start or yarn start (for development)
3. npm run build && serve -s build  (for production build )
(if you have not installed serve command please do  npm install -g serve)
### TECHNOLOGY OR FRAMEWORKS USED ###
 1. REACT (create-react-app)
 2. ANTD (design framework)
 3. REDUX(for state managment)
 4. REACT-ROUTER-DOM (for routing)
 5. REUDX-THUNK (for asynchronous approach)
 6. AXIOS (for handling api calls)
 
 
