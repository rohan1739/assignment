import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import HomePage from "../containers/Home";
import MainApp from "../App";
import HomePage1 from "../containers/Home1";
import HomePage2 from "../containers/Home2";
import HomePage3 from "../containers/Home3";
import HomePage4 from "../containers/Home4";
import HomePage5 from "../containers/Home5";
import HomePage6 from "../containers/Home6";
import NotFound from "../containers/404/404";
export default class Root extends Component {
  render() {
    const RestrictedRoutes = ({ component: Component, ...rest }) => (
      <Route
        {...rest}
        render={props =>
          !this.props.isAuthenticated ? (
            <Component />
          ) : (
            // <Component {...props} />
            <Redirect to={"/"} />
          )
        }
      />
    );
    const MustAuthenticatedRoutes = ({
      component: Component,
      parent: Parent,
      ...rest
    }) => (
      <Route
        {...rest}
        render={props =>
          this.props.isAuthenticated ? (
            Parent ? (
              <Parent>
                <Component {...props} {...rest} />
              </Parent>
            ) : (
              <Component {...props} />
            )
          ) : (
            <Redirect to={"/"} />
          )
        }
      />
    );
    return (
      <Router>
        <MainApp>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/home1" component={HomePage1} />
            <Route exact path="/home2" component={HomePage2} />
            <Route exact path="/home3" component={HomePage3} />
            <Route exact path="/home4" component={HomePage4} />
            <Route exact path="/home5" component={HomePage5} />
            <Route exact path="/home6" component={HomePage6} />
            <Route path="*" component={NotFound} />
          </Switch>
        </MainApp>
       

      </Router>
    );
  }
}
