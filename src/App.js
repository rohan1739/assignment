import React, { Component } from "react";
import { Layout, Input } from "antd";
import "./assets/css/ant-overried.css";
import Sidebar from "./components/Sidebar";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { SearchOutlined } from "@ant-design/icons";
import { setHeaderName } from "./redux/actions";
import { routesArray } from "./constants/common";
const { Header, Content, Footer } = Layout;

class MainApp extends Component {
  componentDidMount() {
    let route = window.location.pathname;
    let pathname = routesArray.find(d => d.path === route);
    if (pathname) {
      this.props.onSetHeaderName(pathname.title);
    } else {
      this.props.history.push("/");
    }
  }
  componentWillReceiveProps(nextProps){
    let route = window.location.pathname;
    let pathname = routesArray.find(d => d.path === route);
    if (pathname) {
      nextProps.onSetHeaderName(pathname.title);
    } else {
      nextProps.history.push("/");
    }
  }
 
  render() {
    let route = window.location.pathname;
    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Sidebar {...this.props} />
        <Layout className="site-layout" style={{ marginLeft: 248 }}>
          <Header className="site-layout-background" style={{ padding: 0 }}>
            <div className="nav-bar-setup">
              <label className="a-link">{this.props.headerName}</label>
              <div className="search-box">
                <Input
                  placeholder="Search"
                  className="search-input"
                  prefix={<SearchOutlined className="site-form-item-icon" />}
                />
                <label className="a-link-1">Ravi</label>
              </div>
            </div>
          </Header>

          {route === "/" && (
            <Content style={{ margin: "25px 16px" }}>
              <div
                className="site-layout-background"
                style={{
                  minHeight: 360,
                  position: "absolute",
                  background: "#F7FAFC",
                  width: "75%",
                  right: "55px",
                  top: "167px",
                  borderRadius: "11px"
                }}
              >
                {this.props.children}
              </div>
            </Content>
          )}
          {route !== "/" && (
            <Content style={{ margin: "25px 16px" }}>
              <div
                className="site-layout-background"
                style={{
                  minHeight: 360,
                  position: "absolute",
                  background: "#F7FAFC",
                  width: "75%",
                  right: "55px",
                  top: "167px",
                  borderRadius: "11px"
                }}
              >
                {this.props.children}
              </div>
            </Content>
          )}
          <Footer style={{ textAlign: "center" }}></Footer>
        </Layout>
      </Layout>
    );
  }
}
const mapStateToProps = ({ assignments }) => {
  return {
    headerName: assignments.headerName
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onSetHeaderName: name => dispatch(setHeaderName(name))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(MainApp));
