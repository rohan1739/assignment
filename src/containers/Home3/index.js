import React, { Component } from "react";
import { Link } from "react-router-dom";

class HomePage3 extends Component {
  render() {
    return (
      <div>
        <div className="home-container">
        <div className="card-heading heading-justify">
            <h1 className="main-head">Home Page 3</h1>
            <Link to="/" className="a-link">
              Go To Assignment Set Up
            </Link>
          </div>
        </div>
        <div className="home-content"></div>
      </div>
    );
  }
}

export default HomePage3;
