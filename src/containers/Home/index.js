import React, { Component } from "react";
import { Row, Col, Button, Table, Pagination, Modal } from "antd";
import { connect } from "react-redux";
import { getAssignments } from "../../redux/actions";
import moment from "moment";
import TextInput from "../../components/InputComponent/TextInput";
import DropDownComponent from "../../components/DropDownComponent/DropdownComponent";
import UtilService from "../../service/ApiService";
import { API_ADD_ASSIGNMENTS, API_URL } from "../../constants/ApiConstants";
import { DeleteTwoTone, ExclamationCircleOutlined } from "@ant-design/icons";
import { ValidateInput } from "./Validations";
import ValidationErrorComponent from "../../components/ValidationErrorComponent/ValidationErrorComponent";
import OpenNotification from "../../components/NotificationBar";
import { GetDesiredDate } from "../../constants/common";

class HomePage extends Component {
  state = {
    offset: 5,
    current: 1,
    assignments: [],
    loading: false,
    programme: null,
    course_list: null,
    units_list: null,
    rego_period: null,
    adding: false,
    errors: {},
    year: null,
    course: "CIB",
    unit_code: "1003 - Miscrosoft Office training",
    exam_code: "",
    weight: "",
    total_marks: "",
    paginationArray: [],
    deleteing: false,
    due_date: "",
    programmeArray: [
      { value: "Cirtificate Courses", label: "Cirtificate Courses" },
      {
        value: "CIB- Cirtificate In Business",
        label: "CIB- Cirtificate In Business"
      }
    ],
    courseListArray: [
      {
        value: "CHRM - Certificate in Human Resourse Management",
        label: "CHRM - Certificate in Human Resourse Management"
      },
      {
        value: "CIA - Certificate in Accounting",
        label: "CIA - Certificate in Accounting"
      }
    ],
    unitListArray: [
      {
        value: "1003 - Certificate in Business",
        label: "1003 - Certificate in Business"
      },
      {
        value: "1002 - Business Communication",
        label: "1002 - Business Communication"
      },
      {
        value: "1012 - Business Accounting",
        label: "1012 - Business Accounting"
      }
    ],
    regoPeriodArray: [
      { value: "Semester 1", label: "Semester 1" },
      { value: "Semester 2", label: "Semester 2" }
    ],
    courseArray: [{ value: "Cirtificate Course", label: "Cirtificate Course" }],
    unitcodeArray: [
      { value: "Cirtificate Course", label: "Cirtificate Course" }
    ]
  };
  componentDidMount() {
    this.props.onFetchAssignments();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.assignments) {
      let assignments = [];
      nextProps.assignments.map((d, index) =>
        assignments.push({ no: index + 1, ...d })
      );
      this.setState({
        assignments: assignments,
        loading: nextProps.loading,
        paginationArray: assignments
      });
    }
  }

  handleChange = e => {
    let errors = null;
    let name = e.target.name;
    let value = e.target.value;
    if (this.state.errors) {
      errors = Object.assign("", this.state.errors);
      delete errors[e.target.name];
    }

    this.setState({ [e.target.name]: e.target.value, errors: errors }, () => {
      if (this.state[name] !== "") {
        let data = {
          [name]: value
        };
        const errors = ValidateInput(data);
        if (!errors.isValid) {
          this.setState({ errors: errors.errors });
        }
      }
    });
  };

  handleDropDown = e => {
    let errors = null;

    if (this.state.errors) {
      errors = Object.assign("", this.state.errors);
      delete errors[e.target.name];
    }

    this.setState({ [e.target.name]: e.target.value, errors: errors }, () => {
      // if (this.state[name] !== "") {
      //   let data = {
      //     [name]: value
      //   };
      //   const errors = ValidateInput(data);
      //   if (!errors.isValid) {
      //     this.setState({ errors: errors.errors });
      //   }
      // }
    });
  };
  clearData = () => {
    this.setState({});
  };
  handleSubmit = e => {
    e.preventDefault();

    this.setState({ adding: true }, () => {
      let data = {
        course_list: this.state.course_list,
        due_date: this.state.due_date,
        exam_code: this.state.exam_code,
        description: this.state.description,
        programme: this.state.programme,
        rego_period: this.state.rego_period,
        total_marks: this.state.total_marks,
        units_list: this.state.units_list,
        weight: this.state.weight,
        year: this.state.year
      };

      const errors = ValidateInput(data);
      // debugger;
      // let date = moment(this.state.due_date).format()
      if (!errors.isValid) {
        this.setState({ errors: errors.errors, adding: false });
      } else {
        const IsoDateTo = moment(this.state.due_date, "DD/MM/YYYY").format(
          "YYYY-MM-DD[T]HH:mm:ss"
        );

        let payload = {
          course: this.state.course_list,
          dueDate: IsoDateTo,
          examcode: parseInt(this.state.exam_code),
          note: this.state.description,
          program: this.state.programme,
          semester: this.state.rego_period,
          totalmarks: this.state.total_marks,
          units: this.state.units_list,
          weight: this.state.weight
        };
        let obj = {
          ...API_ADD_ASSIGNMENTS,
          request: payload
        };

        UtilService.callApi(obj, (err, data) => {
          if (data.status !== 400 && data.status !== 404) {
            let assignments = [];
            data.map((d, index) => assignments.push({ no: index + 1, ...d }));
            this.setState(
              {
                adding: false,
                assignments: assignments,
                paginationArray: assignments,
                year: "",
                due_date: "",
                exam_code: "",
                programme: null,
                rego_period: null,
                total_marks: "",
                units_list: null,
                weight: "",
                description: "",
                course_list: null
              },
              () => {
                document.getElementById("form-id").reset();
                OpenNotification({
                  title: "Assignment Added Successfully",
                  type: "success"
                });
              }
            );
          } else {
            this.setState({ adding: false });
            OpenNotification({ title: data.error, type: "error" });
          }
        });
      }
    });
  };
  handleDelete = assId => {
    Modal.confirm({
      title: "Do you Want To Delete This Assignment Detail",
      icon: <ExclamationCircleOutlined />,
      // okButtonProps: { loading: this.state.deleteing },
      onOk: () => {
        let obj = {
          url: `${API_URL}/ps/assesments/${assId}`,
          method: "delete"
        };
        this.setState({ deleteing: true }, () => {
          UtilService.callApi(obj, (err, data) => {
            if (data) {
              let assignments = [];
              data.map((d, index) => assignments.push({ no: index + 1, ...d }));
              this.setState({
                current:1,
                assignments: assignments,
                paginationArray: assignments,
                deleteing: false
              });
            }
          });
        });
      }
    });
  };
  // handlePagination = (page, pageSize) => {};
  handlePagination = (page_number, page_size) => {
    // human-readable page numbers usually start with 1, so we reduce 1 in the first argument

    let assignments = [];

    this.state.paginationArray.map((d, index) =>
      assignments.push({ no: index + 1, ...d })
    );
    this.setState({
      current: page_number,
      assignments: assignments.slice(
        (page_number - 1) * page_size,
        page_number * page_size
      )
    });
  };
  render() {
    const columns = [
      {
        title: "NO",
        dataIndex: "no"
      },
      {
        title: "ASSIGNMENTS",
        dataIndex: "program"
      },
      {
        title: "WEIGHT",
        dataIndex: "weight"
      },
      {
        title: "MARKS",
        dataIndex: "totalmarks"
      },
      {
        title: "DUE DATE",
        dataIndex: "dueDate",
        render: dt => (dt ? moment(dt).format("DD-MM-YY") : "--")
      },
      {
        title: "Action",
        dataIndex: "assId",
        render: assId => (
          <DeleteTwoTone onClick={() => this.handleDelete(assId)} />
        )
      }
    ];
    let { errors } = this.state;
    return (
      <div>
        <div className="home-container">
          <div className="card-heading">
            <h1 className="main-head">Assignments Setup</h1>
            <h4 className="sub-head">CHECK FOR ASSIGNMENTS SETUP</h4>
          </div>
        </div>
        <div className="home-content">
          <form id="form-id">
            <Row>
              <Col className="custom-form-col" span={8}>
                <DropDownComponent
                  title="Select Programme"
                  name="programme"
                  value={this.state.programme}
                  onChange={this.handleDropDown}
                  className="dropdown-select"
                  optionArray={this.state.programmeArray}
                />
                {errors && (
                  <ValidationErrorComponent
                    message={errors.programme}
                    className="validation-error"
                  />
                )}
              </Col>
              <Col className="custom-form-col" span={8}>
                {/* <div className="dropdown-div">
                <label className="dropdown-lable"></label>
                <select className="dropdown-select">
                  <option>Cirtificate Courses</option>
                  <option>option1</option>
                  <option>option1</option>
                </select>
              </div> */}
                <DropDownComponent
                  title="Course List"
                  name="course_list"
                  value={this.state.course_list}
                  className="dropdown-select"
                  onChange={this.handleDropDown}
                  optionArray={this.state.courseListArray}
                />
                {errors && (
                  <ValidationErrorComponent
                    message={errors.course_list}
                    className="validation-error"
                  />
                )}
              </Col>
              <Col className="custom-form-col" span={8}>
                {/* <div className="dropdown-div">
                <label className="dropdown-lable">Units List</label>
                <select className="dropdown-select">
                  <option>Cirtificate Courses</option>
                  <option>option1</option>
                  <option>option1</option>
                </select>
              </div> */}
                <DropDownComponent
                  title="Units List"
                  name="units_list"
                  value={this.state.units_list}
                  className="dropdown-select"
                  onChange={this.handleDropDown}
                  optionArray={this.state.unitListArray}
                />
                {errors && (
                  <ValidationErrorComponent
                    message={errors.units_list}
                    className="validation-error"
                  />
                )}
              </Col>
            </Row>
            <Row>
              <Col className="custom-form-col" span={8}>
                {/* <div className="dropdown-div">
                <label className="dropdown-lable">Rego Period</label>
                <select className="dropdown-select">
                  <option>Cirtificate Courses</option>
                  <option>option1</option>
                  <option>option1</option>
                </select>
              </div> */}
                <DropDownComponent
                  title="Rego Period"
                  name="rego_period"
                  value={this.state.rego_period}
                  className="dropdown-select"
                  onChange={this.handleDropDown}
                  optionArray={this.state.regoPeriodArray}
                />
                {errors && (
                  <ValidationErrorComponent
                    message={errors.rego_period}
                    className="validation-error"
                  />
                )}
              </Col>
              <Col className="custom-form-col" span={4}>
                <TextInput
                  className="input-yr"
                  name="year"
                  title="Year"
                  value={this.state.year}
                  onChange={this.handleChange}
                  onBlur={() => {}}
                />
                {errors && (
                  <ValidationErrorComponent
                    message={errors.year}
                    className="validation-error"
                  />
                )}
              </Col>
              <Col className="custom-form-col" span={6}>
                <div className="dropdown-div">
                  <label className="dropdown-lable">Course</label>
                  <label className="dropdown-lable">{this.state.course}</label>
                </div>
              </Col>
              <Col className="custom-form-col" span={6}>
                <div className="dropdown-div">
                  <label className="dropdown-lable">Unit Code</label>
                  <label className="dropdown-lable">
                    {this.state.unit_code}
                  </label>
                </div>
              </Col>
            </Row>
            <Row>
              <Col className="custom-form-col" span={6}>
                {/* <div className="dropdown-div">
                <label className="dropdown-lable">Rego Period</label>
                <input className="input-exam-code" />
              </div> */}
                <TextInput
                  className="input-exam-code"
                  name="exam_code"
                  title="Exam Code"
                  value={this.state.exam_code}
                  onChange={this.handleChange}
                  onBlur={() => {}}
                />
                {errors && (
                  <ValidationErrorComponent
                    message={errors.exam_code}
                    className="validation-error"
                  />
                )}
              </Col>
              <Col className="custom-form-col" span={6}>
                <TextInput
                  className="input-exam-code"
                  name="weight"
                  title="Weight"
                  value={this.state.weight}
                  onChange={this.handleChange}
                  onBlur={() => {}}
                />
                {errors && (
                  <ValidationErrorComponent
                    message={errors.weight}
                    className="validation-error"
                  />
                )}
              </Col>
              <Col className="custom-form-col" span={6}>
                {/* <div className="dropdown-div">
                <label className="dropdown-lable">Course</label>
                <input className="input-exam-code" />
              </div> */}
                <TextInput
                  className="input-exam-code"
                  name="total_marks"
                  title="Total Marks"
                  value={this.state.total_marks}
                  onChange={this.handleChange}
                  onBlur={() => {}}
                />
                {errors && (
                  <ValidationErrorComponent
                    message={errors.total_marks}
                    className="validation-error"
                  />
                )}
              </Col>
              <Col className="custom-form-col" span={6}>
                {/* <div className="dropdown-div">
                <label className="dropdown-lable">Unit Code</label>
                <input className="input-exam-code" />
              </div> */}
                <TextInput
                  className="input-exam-code"
                  name="due_date"
                  title="Due Date"
                  value={this.state.due_date}
                  onChange={this.handleChange}
                  onBlur={() => {}}
                />
                <div className="dropdown-div">
                  {" "}
                  {errors && (
                    <ValidationErrorComponent
                      message={errors.due_date}
                      className="validation-error"
                    />
                  )}
                  <label className="dropdown-lable">eg 22-10-2010</label>
                </div>
              </Col>
            </Row>
            <Row>
              <Col className="custom-form-col" span={24}>
                <div className="dropdown-div">
                  <label className="dropdown-lable">Desciption</label>
                  <textarea
                    style={{ padding: 10 }}
                    className="input-desicription"
                    name="description"
                    value={this.state.description}
                    onChange={this.handleChange}
                  />
                </div>
              </Col>
            </Row>
            <div className="btn-div">
              <Button
                loading={this.state.adding}
                onClick={this.handleSubmit}
                className="add-btn"
              >
                Add New
              </Button>
            </div>
          </form>
        </div>

        <div className="assignment-tbl-wrapper">
          {this.state.paginationArray && (
            <label className="total-span">
              Total : {this.state.paginationArray.length}
            </label>
          )}
          <Table
            className="assignment-tbl"
            dataSource={this.state.assignments.slice(0, 5)}
            columns={columns}
            loading={this.props.loading || this.state.deleteing}
            pagination={false}
            // scroll={{ y: 300 }}
          />
          {
            <div className={"pagination-wrapper"}>
              <Pagination
                defaultCurrent={1}
                current={this.state.current}
                onChange={this.handlePagination}
                pageSize={5}
                total={this.state.paginationArray.length}
              />
            </div>
          }
        </div>
      </div>
    );
  }
}
const mapStateToProps = ({ assignments }) => {
  return {
    assignments: assignments.assignmentData,
    loading: assignments.loading
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onFetchAssignments: () => dispatch(getAssignments())
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
