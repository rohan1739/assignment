import isEmpty from "lodash/isEmpty";
import validator from "validator";
import moment from "moment";
export function ValidateInput(data) {
  let errors = {};

  if (data.programme !== undefined && isEmpty(data.programme)) {
    errors.programme = "Select Programme";
  }
  if (data.course_list !== undefined && isEmpty(data.course_list)) {
    errors.course_list = "Select Course";
  }

  if (data.units_list !== undefined && isEmpty(data.units_list)) {
    errors.units_list = "Select Units";
  }
  if (data.rego_period !== undefined && isEmpty(data.rego_period)) {
    errors.rego_period = "Select Rego Period";
  }
  if (data.year !== undefined && isEmpty(data.year)) {
    errors.year = "Enter Year";
  }
  if (!isEmpty(data.year)) {
    if (!moment(data.year, "YYYY", true).isValid()) {
      errors.year = "Enter a Valid Year";
    }
  }
  if (data.exam_code !== undefined && isEmpty(data.exam_code)) {
    errors.exam_code = "Enter a Exam Code";
  }
  if (
    !isEmpty(data.exam_code) &&
    validator.isNumeric(data.exam_code) === false
  ) {
    errors.exam_code = "Exam Code  Should Be A Numeric Value";
  }
  if (data.weight !== undefined && isEmpty(data.weight)) {
    errors.weight = "Enter Weight";
  }
  if (!isEmpty(data.weight) && validator.isNumeric(data.weight) === false) {
    errors.weight = "Weight Should Be A Numeric Value";
  }
  if (data.total_marks !== undefined && isEmpty(data.total_marks)) {
    errors.total_marks = "Enter Total Marks";
  }
  if (
    !isEmpty(data.total_marks) &&
    validator.isNumeric(data.total_marks) === false
  ) {
    errors.total_marks = "Total Marks Should Be A Numeric Value";
  }
  if (data.due_date !== undefined && isEmpty(data.due_date)) {
    errors.due_date = "Select Due Date";
  }

  if (!isEmpty(data.due_date)) {
    let date = data.due_date;
    // if (!moment(date).isValid()) {
      // let new_string = date.split(/[ -/]/);
      // var date_str = new_string[1] + "/" + new_string[0] + "/" + new_string[2];
      if (
        !moment(data.due_date, "DD-MM-YYYY", true).isValid() &&
        !moment(data.due_date, "DD-MM-YYYY", true).isValid()
      ) {
        errors.due_date = "Invalid Date";
      }
    // }
  }

  return { errors, isValid: isEmpty(errors) };
}
