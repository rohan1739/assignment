import React, { Component } from "react";
import { Layout, Menu, Breadcrumb } from "antd";
import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined
} from "@ant-design/icons";
import "./sidebar.css";
import Logo from "../../assets/img/canyon_logo.png";
import MenuComponent from "../Menu";
import MenuData from "../../components/Menu/MenuData";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
export default class Sidebar extends Component {
  state = {
    collapsed: false
  };

  onCollapse = collapsed => {
    this.setState({ collapsed });
  };
  render() {
    return (
      <Sider
        trigger={null}
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
          width: "248px !important",
          maxWidth: "248px !important"
        }}
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}
      >
        <div className="logo" >
           <img src={Logo} alt="" />
        </div>
        <MenuComponent {...this.props} MenuData={MenuData} />
      </Sider>
    );
  }
}
