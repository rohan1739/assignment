import React from "react";
import PropTypes from "prop-types";

const TextInput = props => {
  return (
    <div className="dropdown-div">
      <label className="dropdown-lable">{props.title}</label>
      <input
        type="text"
        placeholder={props.placeholder}
        onChange={props.onChange}
        name={props.name}
        onBlur={props.onBlur}
        value={props.value}
        className={props.className}
      />
      {/* <TextInput className="input-exam-code" /> */}
    </div>
  );
};

TextInput.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func,
  ref: PropTypes.func,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
};
export default TextInput;
