import React from "react";
import PropTypes from "prop-types";

const DropDownComponent = props => {
  return (
    <div className="dropdown-div">
      <label className="dropdown-lable">{props.title}</label>
      <select
        name={props.name}
        onChange={props.onChange}
        value={props.value}
        className={props.className}
      >
        <option value={null}>Select</option>
        {props.optionArray &&
          props.optionArray.length !== 0 &&
          props.optionArray.map(d => {
            return <option value={d.value}>{d.label}</option>;
          })}
      </select>
    </div>
  );
};

DropDownComponent.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  optionArray: PropTypes.array.isRequired
};
export default DropDownComponent;
