import React, { Component } from "react";
import { Menu } from "antd";
import PropTypes from "prop-types";
import { HomeFilled } from "@ant-design/icons";
import { routesArray } from "../../constants/common";
const { SubMenu } = Menu;

export default function MenuComponent(props) {
  let MenuData = props.MenuData;
  return (
    <Menu theme="light" defaultSelectedKeys={["1"]} mode="inline">
      {MenuData.map((menu, index) => {
        if (menu.child && menu.child.length !== 0) {
          return (
            <SubMenu
              key={menu.index}
              title={
                <span>
                  {/* <UserOutlined /> */}
                  {menu.icon}
                  <span>{menu.title}</span>
                </span>
              }
            >
              {menu.child.map((d, index2) => {
                return (
                  <Menu.Item
                    onClick={menu => {
                      if (!d.route.includes("registration")) {
                        props.history.push(d.route);
                        props.onSetHeaderName(d.title);
                      }
                    }}
                    key={d.index}
                  >
                    {d.icon}
                    {d.title}
                  </Menu.Item>
                );
              })}
            </SubMenu>
          );
        } else {
          return (
            <Menu.Item key={index}>
              {menu.icon}
              <span>{menu.title}</span>
            </Menu.Item>
          );
        }
      })}
    </Menu>
  );
}

MenuComponent.propTypes = {
  MenuData: PropTypes.array.isRequired
};
