import React from "react";
import {
  HomeFilled,
  UserOutlined,
  MobileOutlined,
  MoneyCollectFilled,
  LikeFilled,
  UnlockOutlined,
  FileTextOutlined,
  PercentageOutlined,
  PrinterFilled,
  IdcardFilled,
  EditFilled,
  ToolFilled
} from "@ant-design/icons";

const MenuArray = [
 
  {
    title: "Home",
    index: "main1",
    icon: <HomeFilled />,
    child: [
      {
        title: "Home 1",
        icon: <HomeFilled />,
        route: "/home1",
        index: "sub1"
      },
      {
        title: "Home 2",
        index: "sub2",
        icon: <HomeFilled />,
        route: "/home2"
      },
      {
        title: "Home 3",
        index: "sub3",
        icon: <HomeFilled />,
        route: "/home3"
      },
      {
        title: "Home 4",
        index: "sub4",
        icon: <HomeFilled />,
        route: "/home4"
      },
      {
        title: "Home 5",
        index: "sub5",
        icon: <HomeFilled />,
        route: "/home5"
      },
      {
        title: "Home 6",
        index: "sub6",
        icon: <HomeFilled />,
        route: "/home6"
      }
    ]
  },

  {
    title: "Student Registration",
    index: "main2",

    icon: <UserOutlined />,
    child: [
      {
        title: "Registration1",
        icon: <UserOutlined />,
        index: "reg1",
        route: "/registration1"
      },
      {
        title: "Registration2",
        index: "reg2",
        icon: <UserOutlined />,
        route: "/registration12"
      },
      {
        title: "Registration3",
        index: "reg3",

        icon: <UserOutlined />,
        route: "/registration13"
      }
    ]
  },
  {
    title: "Course Applications",
    index: "main3",
    icon: <MobileOutlined />,
    child: []
  },
  {
    title: "Payment Batch",
    index: "main4",

    icon: <MoneyCollectFilled />,
    child: []
  },
  {
    title: "Receipts And Refunds",
    index: "main5",

    icon: <LikeFilled />,
    child: []
  },
  {
    title: "Student Accounts",
    index: "main6",

    icon: <UnlockOutlined />,
    child: []
  },
  {
    title: "Assignments Data Entry",
    index: "main7",

    icon: <FileTextOutlined />,
    child: []
  },
  {
    title: "Course Grades",
    index: "main8",

    icon: <PercentageOutlined />,
    child: []
  },
  {
    title: "Printing of Transcripts",
    index: "main9",

    icon: <PrinterFilled />,
    child: []
  },
  {
    title: "Printing of Certificates",
    index: "main10",

    icon: <PrinterFilled />,
    child: []
  },
  {
    title: "Student ID Cards",
    index: "main11",

    icon: <IdcardFilled />,
    child: []
  },
  {
    title: "Account General Entries",
    index: "main12",

    icon: <EditFilled />,
    child: []
  },
  {
    title: "User Maintanance",
    index: "main13",

    icon: <ToolFilled />,
    child: []
  },
  {
    title: "Setting Up of Course Detail",
    index: "main14",

    icon: <LikeFilled />,
    child: []
  },
  {
    title: "Setting up Grades",
    index: "main15",

    icon: <EditFilled />,
    child: []
  }
];

export default MenuArray;
